console.log(`Soal No. 1 (Array to Object)`)
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

arrayToObject = (arr) => {
    // console.log(typeof arr)
    // console.log(arr.length)
    if(arr.length==0){
        console.log(arr)
    } else {
        for(j=0;j<arr.length;j++){
            obj={}
            switch(i=0){
                case 0:{obj.firstName = arr[j][i];i++}
                case 1:{obj.lastName = arr[j][i];i++}
                case 2:{obj.gender = arr[j][i];i++}
                case 3:{
                    if(arr[j][i]==undefined || arr[j][i] > thisYear){
                        obj.male = `Invalid Birth Year`
                    } else {
                        obj.male = thisYear - arr[j][i]
                    }
                }
            }
            // console.log(`${j+1}.`, obj.firstName, obj.lastName, ':', obj)
            console.log(`${j+1}. ${obj.firstName} ${obj.lastName}:`, obj)
            // console.log(obj)
        }
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// console.log(`\n`)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log(`\nSoal No. 2 (Shopping Time)`)
shoppingTime = (memberId, money) => {
    obj={}
    listPurchased = []
    obj.memberId = memberId
    obj.money = money

    if(memberId==undefined || memberId===''){
        return `Mohon maaf, toko X hanya berlaku untuk member saja`
    } else if (money<50000){
        return `Mohon maaf, uang tidak cukup`
    } else {
        switch(true){
            case(money>1500000):{money-=1500000; listPurchased.push('Sepatu Stacattu')}
            case(money>500000):{money-=500000; listPurchased.push('Baju Zoro')}
            case(money>250000):{money-=250000; listPurchased.push('Baju H&N')}
            case(money>175000):{money-=175000; listPurchased.push('Sweater Uniklooh')}
            case(money>50000):{money-=50000; listPurchased.push('Casing Handphone')}
        }
        obj.listPurchased = listPurchased
        obj.changeMoney = money
    }
    return obj
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
    //{ memberId: '82Ku8Ma742',
    // money: 170000,
    // listPurchased:
    //  [ 'Casing Handphone' ],
    // changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(`\nSoal No. 3 (Naik Angkot)`)
naikAngkot=(arrPenumpang)=>{
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  listPenumpang = []
  if(arrPenumpang.length==0){
      return arrPenumpang
  } else {
    for(j=0;j<arrPenumpang.length;j++){
        obj={}
        switch(i=0){
            case 0:{obj.penumpang = arrPenumpang[j][i];i++}
            case 1:{obj.naikDari = arrPenumpang[j][i];i++}
            case 2:{obj.tujuan = arrPenumpang[j][i];i++}
            case 3:{
                for(k=0;arrPenumpang[j][1]!==rute[k];k++){}
                for(l=0;arrPenumpang[j][2]!==rute[l];l++){}
                obj.bayar = (l - k)*2000
            }
        }
        listPenumpang.push(obj)
    }
  }
  return listPenumpang
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]
