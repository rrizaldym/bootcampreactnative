let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let i=0
let waktu = 10000
bacabuku = () => {
    readBooksPromise(waktu, books[i])
        .then((sisaWaktu) => {
            i++
            waktu=sisaWaktu
            bacabuku()
        })
        .catch(()=>{
            console.log(`buku habis`)
        })
}

bacabuku()