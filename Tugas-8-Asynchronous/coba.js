// function periksaDokter(nomerAntri, callback) {
//     if(nomerAntri > 50 ) {
//         callback(false)
//     } else if(nomerAntri < 10) {
//         callback(true)
//     }    
// } 

// periksaDokter(5, function(check) {
//     if(check) {
//         console.log("sebentar lagi giliran saya")
//     } else {
//         console.log("saya jalan-jalan dulu")
//     }
// }) 

var isMomHappy = true;
 
// Promise
let willIGetNewPhone = new Promise(
    function (resolve, reject) {
        if (isMomHappy) {
            let phone = {
                brand: 'Samsung',
                color: 'black'
            };
            resolve(phone); // fulfilled atau janji dipenuhi
        } else {
            let reason = new Error('mom is not happy');
            reject(reason); // reject (ingkar)
        }
 
    }
); 
// Menjalankan Promise 
// Menjalankan promise seperti kita menagih janji yang sudah dibuat.

function askMom() {
    willIGetNewPhone
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
         // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
         // output: 'mom is not happy'
        });
}
 
// Tanya Mom untuk menagih janji
askMom() 

