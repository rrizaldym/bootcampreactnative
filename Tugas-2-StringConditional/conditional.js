console.log(`if-else`)
console.log(`Output untuk Input nama = '' dan peran = ''`)
var nama = ""
var peran = ""
if (nama===''){
    console.log(`Nama harus diisi!`)
}else if(peran===''){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
}else if(peran==='Penyihir'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
}else if(peran==='Guard'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
}else if(peran==='Werewolf'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`)
}

console.log(`\nOutput untuk Input nama = 'John' dan peran = ''`)
var nama = "John"
var peran = ""
if (nama===''){
    console.log(`Nama harus diisi!`)
}else if(peran===''){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
}else if(peran==='Penyihir'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
}else if(peran==='Guard'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
}else if(peran==='Werewolf'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`)
}

console.log(`\nOutput untuk Input nama = 'Jane' dan peran 'Penyihir'`)
var nama = "Jane"
var peran = "Penyihir"
if (nama===''){
    console.log(`Nama harus diisi!`)
}else if(peran===''){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
}else if(peran==='Penyihir'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
}else if(peran==='Guard'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
}else if(peran==='Werewolf'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`)
}

console.log(`\nOutput untuk Input nama = 'Jenita' dan peran 'Guard'`)
var nama = "Jenita"
var peran = "Guard"
if (nama===''){
    console.log(`Nama harus diisi!`)
}else if(peran===''){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
}else if(peran==='Penyihir'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
}else if(peran==='Guard'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
}else if(peran==='Werewolf'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`)
}

console.log(`\nOutput untuk Input nama = 'Junaedi' dan peran 'Werewolf'`)
var nama = "Juanedi"
var peran = "Werewolf"
if (nama===''){
    console.log(`Nama harus diisi!`)
}else if(peran===''){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
}else if(peran==='Penyihir'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
}else if(peran==='Guard'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
}else if(peran==='Werewolf'){
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`)
}

console.log(`\nSwitch Case`)
console.log(`Bisa membedakan tahun kabisat`)
var tanggal = 21; 
var bulan = 1; 
var tahun = 1945;

switch(true){
    case(tahun<1900 || tahun>2200):{console.log(`assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)`);break;}
    case(tahun%4==0):{
        switch(true){
            case(bulan<1 || bulan>12):{console.log(`assign nilai variabel bulan disini! (dengan angka antara 1 - 12)`);break;}
            case(bulan==1):{
                bulan='Januari'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==2):{
                bulan='Februari'
                switch(true){
                    case(tanggal<1 || tanggal>29):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} (Kabisat) disini! (dengan angka antara 1 - 29)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==3):{
                bulan='Maret'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==4):{
                bulan='April'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==5):{
                bulan='Mei'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==6):{
                bulan='Juni'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==7):{
                bulan='Juli'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==8):{
                bulan='Agustus'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==9):{
                bulan='September'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==10):{
                bulan='Oktober'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==11):{
                bulan='November'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==12):{
                bulan='Desember'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
        }
    }
    default:{
        switch(true){
            case(bulan<1 || bulan>12):{console.log(`assign nilai variabel bulan disini! (dengan angka antara 1 - 12)`);break;}
            case(bulan==1):{
                bulan='Januari'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==2):{
                bulan='Februari'
                switch(true){
                    case(tanggal<1 || tanggal>28):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 28)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==3):{
                bulan='Maret'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==4):{
                bulan='April'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==5):{
                bulan='Mei'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==6):{
                bulan='Juni'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==7):{
                bulan='Juli'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==8):{
                bulan='Agustus'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==9):{
                bulan='September'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==10):{
                bulan='Oktober'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==11):{
                bulan='November'
                switch(true){
                    case(tanggal<1 || tanggal>30):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 30)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
            case(bulan==12):{
                bulan='Desember'
                switch(true){
                    case(tanggal<1 || tanggal>31):{console.log(`assign nilai variabel tanggal untuk bulan ${bulan} disini! (dengan angka antara 1 - 31)`);break;}
                    default:{console.log(`${tanggal} ${bulan} ${tahun}`);break;}
                }
                break
            }
        }
    }
}