console.log(`Soal No. 1 (Range)`)
range = (startNum, finishNum) => {
    arr = []
    if(startNum==undefined||finishNum==undefined){
        return -1
    } else if (startNum>finishNum) {
        for(i=startNum;i>=finishNum;i--){
            arr.push(i)
        }
    } else {
        for(i=startNum;i<=finishNum;i++){
            arr.push(i)
        }
    }
    return arr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log(`\nSoal No. 2 (Range with Step)`)
rangeWithStep = (startNum, finishNum, step) => {
    arr= []
    if(startNum==undefined||finishNum==undefined){
        return -1
    } else if (startNum>finishNum) {
        for(i=startNum;i>=finishNum;i-=step){
            arr.push(i)
        }
    } else {
        for(i=startNum;i<=finishNum;i+=step){
            arr.push(i)
        }
    }
    return arr
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(`\nSoal No. 3 (Sum of Range)`)
sum = (startNum, finishNum, step) => {
    arr = []
    if(startNum==undefined){
        return 0
    } else if (finishNum==undefined) {
        return startNum
    } else if (step==undefined) {
        if(startNum<finishNum){
            for(i=startNum;i<=finishNum;i++){
                arr.push(i)
            }
        }else{
            for(i=startNum;i>=finishNum;i--){
                arr.push(i)
            }
        }
    } else {
        if(startNum<finishNum){
            for(i=startNum;i<=finishNum;i+=step){
                arr.push(i)
            }
        }else{
            for(i=startNum;i>=finishNum;i-=step){
                arr.push(i)
            }
        }
    }
    var total = 0
    for (i=0;i<arr.length;i++){
        total+=arr[i]
    }
    return total
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log(`\nSoal No. 4 (Array Multidimensi)`)
dataHandling = (input) => {
    // arr = []
    // for (j=0;j<input.length;j++){
    //     switch(k=0){
    //         case 0:{arr.push(`Nomor ID: ${input[j][k]}`); k++}
    //         case 1:{arr.push(`Nama Lengkap: ${input[j][k]}`); k++}
    //         case 2:{arr.push(`TTL: ${input[j][k]} ${input[j][k]+1}`); k+=2}
    //         default:{arr.push(`Hobi: ${input[j][k]}`);arr.push(``)}
    //     }
    // }
    // text = ``
    // for(i=0;i<arr.length;i++){
    //     text+=`${arr[i]}\n`
    // }
    text = ``
    for(j=0;j<input.length;j++){
        switch(k=0){
            case 0:{text+=`Nomor ID: ${input[j][k]}\n`;k++}
            case 1:{text+=`Nama Lengkap: ${input[j][k]}\n`;k++}
            case 2:{text+=`TTL: ${input[j][k]}, ${input[j][k+1]} \n`;k+=2}
            default:{text+=`Hobi: ${input[j][k]}\n\n`}
        }
    }
    return text
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log(dataHandling(input))

console.log(`\nSoal No. 5 (Balik Kata)`)
balikKata = (input) => {
    balik = ``
    for(i=input.length-1;i>=0;i--){
        balik+=`${input[i]}`
    }
    return balik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log(`\nSoal No. 6 (Metode Array)`)
dataHandling2 = (input) => {
    input.splice(1,4,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(input)

    tanggal = input[3].split("/")
    switch(tanggal[1]){
        case ('01'):{console.log('Januari');break;}
        case ('02'):{console.log('Februari');break;}
        case ('03'):{console.log('Maret');break;}
        case ('04'):{console.log('April');break;}
        case ('05'):{console.log('Mei');break;}
        case ('06'):{console.log('Juni');break;}
        case ('07'):{console.log('Juli');break;}
        case ('08'):{console.log('Agustus');break;}
        case ('09'):{console.log('September');break;}
        case ('10'):{console.log('Oktober');break;}
        case ('11'):{console.log('November');break;}
        case ('12'):{console.log('Desember');break;}
    }

    console.log(tanggal.sort(function (x,y){return y-x}))

    console.log(input[3].split('/').join('-'))

    console.log(`${input[1]}`.slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
