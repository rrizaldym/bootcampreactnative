console.log(`No. 1 Looping While`)
var i = 0
console.log(`LOOPING PERTAMA`)
while(i<20){
    i+=2
    console.log(`${i} - I love coding`)
}
console.log(`LOOPING KEDUA`)
while(i>0){
    console.log(`${i} - I will become a mobile developer`)
    i-=2
}

console.log(`\nNo. 2 Looping menggunakan for`)
var ganjil = ' - Santai'
var genap = ' - Berkualitas'
var kelipatan3 = ' - I Love Coding'
for(var i = 1; i <= 20; i++){
    if(i%3==0 && i%2!=0){
        console.log(i+kelipatan3)
    } else if (i%2==0){
        console.log(i+genap)
    }else {
        console.log(i+ganjil)
    }
}

console.log(`\nNo. 3 Membuat Persegi Panjang #`)
for(var i = 1; i <= 4; i++){
    console.log('########')
}

console.log(`\nNo. 4 Membuat Tangga`)
var pagar = '#'
for(var i = 1; i <=7; i++){
    console.log(pagar)
    pagar+='#'
}

console.log(`\nNo. 5 Membuat Papan Catur`)
var ganjil = ' # # # #'
var genap = '# # # # '
for(var i = 1; i <=8; i++){
    if(i%2==0){
        console.log(genap)
    }else {
        console.log(ganjil)
    }
}