import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import {NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import Register from './Register'
import Login from './LoginScreen'
import About from './AboutScreen'

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tab = createBottomTabNavigator()

const MainScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name='About' component={About}/>
        <Drawer.Screen name='Login' component={Login}/>
    </Drawer.Navigator>
)

const SplashScreen = () => (
    <Tab.Navigator>
        <Tab.Screen name='Register' component={Register}/>
        <Tab.Screen name='Login' component={Login}/>
    </Tab.Navigator>
)

export default function index() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Spalsh' component={SplashScreen}/>
                <Stack.Screen name='MainScreen' component={MainScreen} options={ {headerTitle:'Drawer'}}/>
                <Stack.Screen name='Login' component={Login}/>
                <Stack.Screen name='Register' component={Register}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})
