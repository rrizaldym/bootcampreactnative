import React from 'react';
import Register from './Register'
import LoginScreen from './LoginScreen'
import AboutScreen from './AboutScreen'
import { ScrollView, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <ScrollView>
        <Register/>
        <LoginScreen/>
        <AboutScreen/>
      </ScrollView>
    )
  }
}
