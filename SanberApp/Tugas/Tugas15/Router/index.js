import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {createDrawerNavigator} from '@react-navigation/drawer'

import AboutScreen from '../Pages/AboutScreen'
import AddScreen from '../Pages/AddScreen'
import Home from '../Pages/Home'
import Login from '../Pages/Login'
import ProjectScreen from '../Pages/ProjectScreen'
import Setting from '../Pages/Setting'
import SkillProject from '../Pages/SkillProject'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()
const Drawer = createDrawerNavigator()

const MainApp = () => (

        <Tab.Navigator>
            <Tab.Screen name='AboutScreen' component={AboutScreen} />
            <Tab.Screen name='AddScreen' component={AddScreen} />
            <Tab.Screen name='SkillProject' component={SkillProject} />
        </Tab.Navigator>
)

const MyDrawer = () => (
        <Drawer.Navigator>
            <Tab.Screen name='App' component={MainApp} />
            <Tab.Screen name='AboutScreen' component={AboutScreen} />
        </Drawer.Navigator>
)

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Login' component={Login} />
                <Stack.Screen name='Home' component={Home} />
                <Stack.Screen name='MainApp' component={MainApp} />
                <Stack.Screen name='MyDrawer' component={MyDrawer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})
