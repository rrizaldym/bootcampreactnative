import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import Tugas13 from './Tugas/Tugas13/index'
// import Tugas15 from './Tugas/Tugas15/Index'

import Quiz3 from './Tugas/Quiz3/index'

export default function App() {
  return (
      // <Tugas15 />
    // <Tugas13/>
    <Quiz3/>
    
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});